// 
// active and tab Sản phẩm và dịch vụ  
function openTab(event, tabId) {
    // Lấy tất cả các phần tử tab và nội dung
    const tabButtons = document.querySelectorAll('.tab-button');
    // const tabTẽs = document.querySelectorAll('.tab-text');
    const tabContents = document.querySelectorAll('.tab-content');

    // Loại bỏ lớp active từ tất cả các nút tab
    tabButtons.forEach(button => {
        button.classList.remove('active');
        button.style.color = '';
    });

    // Thêm lớp active cho nút tab được chọn
    event.currentTarget.classList.add('active');
    event.currentTarget.style.color = '#000'; 

    // Ẩn tất cả nội dung tab
    tabContents.forEach(content => {
        content.style.display = 'none';
    });

    // Hiển thị nội dung tab được chọn
    document.getElementById(tabId).style.display = 'block';

   
}

// Hiển thị nội dung của tab 1 mặc định khi trang được tải
document.addEventListener('DOMContentLoaded', function () {
    document.getElementById('tab1').style.display = 'block';
    document.querySelector('.tab-button.active').classList.add('active');
});