// menu
const tabLinks = document.querySelectorAll('.tab-link');

tabLinks.forEach(link => {
    link.addEventListener('click', () => {
        // Loại bỏ lớp active từ tất cả các tab-link
        tabLinks.forEach(tabLink => {
            tabLink.classList.remove('active');
        });
        
        // Thêm lớp active cho tab-link được chọn
        link.classList.add('active');
    });
});
// end menu


// Hàm cuộn lên đầu trang
function scrollToTop() {
    window.scrollTo({
        top: 0,
        behavior: 'smooth' // Sử dụng cuộn mượt
    });
}

// Hiển thị hoặc ẩn nút Back to Top dựa trên vị trí trang
window.onscroll = function() {
    var scrollPosition = document.documentElement.scrollTop || document.body.scrollTop;
    var backToTopButton = document.getElementById('backToTopBtn');

    if (scrollPosition >= 500) {
        backToTopButton.style.display = 'block';
    } else {
        backToTopButton.style.display = 'none';
    }
}